import { useState } from "react";
import { Button, Container, Form, Stack } from "react-bootstrap";
import './App.css';

function App() {
  const [ans, setAns] = useState(0);
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);

  const add = () => {
    setAns(parseInt(num1) + parseInt(num2));
  };
  const subtract = () => {
    setAns(parseInt(num1) - parseInt(num2));
  };
  const multiply = () => {
    setAns(parseInt(num1) * parseInt(num2));
  };
  const divide = () => {
    if(num2 !== 0){
      setAns(parseInt(num1) / parseInt(num2));
    }
    else{
      setAns("Can't divide to zero.");
    }
  };
  const reset = () => {
    setAns(0);
    setNum1(0);
    setNum2(0);
  };


  return (
    <Container className="text-center">
      <h1 className="mt-3 mb-5">Calculator</h1>
      <h1 className="mt-3 mb-5">{ans}</h1>
      <Stack direction="horizontal" className="mt-5 mb-5">
        <Form.Control
          type="number"
          value={num1}
          onChange={e => setNum1(e.target.value)}
        />
        <Form.Control
          type="number"
          value={num2}
          onChange={e => setNum2(e.target.value)}
        />
      </Stack>
      <Button 
        variant="success"
        onClick={add}
      >
        Add</
      Button>
      <Button 
        variant="success"
        onClick={subtract}
      >
        Subtract</Button>
      <Button 
        variant="success"
        onClick={multiply}
      >
        Multiply</Button>
      <Button 
        variant="success"
        onClick={divide}
      >
        Divide</Button>
      <Button 
        variant="success"
        onClick={reset}
      >
        Reset
      </Button>
    </Container>
  );
}

export default App;
